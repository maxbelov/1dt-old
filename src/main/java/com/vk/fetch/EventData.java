package com.vk.fetch;

import java.util.List;

public class EventData {

	public String description;
	public int attendeesCount;
	public String linkToPhotoAlbum;
	public String linkToAttendeeList;
	protected List<String> photoLinks;
	protected List<String> attendeeLinks;
	public String profileImage;
	public String website;
	public String startTime;
	public String endTime;
	public String location;

}
