package com.vk.fetch;

import java.io.IOException;
import java.util.*;

import javafx.scene.web.WebEngine;

import netscape.javascript.JSException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import netscape.javascript.JSObject;

public class ContentFSM {
	enum State {
		NULL, AUTHENTICATE, LOAD_LINKS, LOAD_EVENTS, LOAD_PHOTOS, LOAD_ATTENDEES
	}
	enum Event {
		NULL, BROWSE, INIT, LOADED, PARSE_LINKS
	}
	enum Action {
		NULL, WAIT, CONTINUE;
	}
	
	private static class NextData {
		public State state = State.NULL;
		public Event event = Event.NULL;
		public Action action = Action.NULL;
		public Object eventData = null;
	}
	
	private WebEngine webEngine;
	private NextData next = new NextData();
	Map<State, Map<Event, EventHandler>> handlers = new HashMap<State, Map<Event, EventHandler>>();
	
	EventData currentEventData = null;

	public ContentFSM(WebEngine webEngine) {
		this.webEngine = webEngine;
	}
	
	// page is loaded
	public void pageLoaded() {
		next.event = Event.LOADED;
		next.action = Action.CONTINUE;
		run();
	}
	
	public void run() {
		//TODO: CONSUME ACTION?
		if (next.action != Action.CONTINUE) {
			throw new RuntimeException("NEXT ACTION SHOULD BE CONTINUE TO RUN");
		}
		do {
			EventHandler handler = handlers.get(next.state).get(next.event);
			System.out.println("Executing handler - State: " + next.state + ", Event: " + next.event);
			handler.execute();
		} while (next.action == Action.CONTINUE);
	}
	
	{
		//TODO: move to EventHandler execute parameter and return it?
		next.state = State.AUTHENTICATE;
		next.event = Event.INIT;
		next.action = Action.CONTINUE;
		
		
		registerEventHandler(Event.BROWSE, new EventHandler() {
			@Override
			public void execute() {
				String url = (String) next.eventData;
				System.out.println("BROWSE TO " + url);
				webEngine.load(url);
				next.action = Action.WAIT;
			}
		});
		
		registerEventHandler(State.AUTHENTICATE, Event.INIT, new EventHandler() {

			@Override
			public void execute() {
				next.event = Event.BROWSE;
				next.action = Action.CONTINUE;
				next.eventData = "http://m.vk.com";
			}
		});
		
		registerEventHandler(State.AUTHENTICATE,  Event.LOADED, new EventHandler() {
			
			@Override
			public void execute() {
				Document doc = webEngine.getDocument();
				
				if (doc != null) {
		        	Element email = null;
		        	Element pass = null;
		        	NodeList inputs = doc.getElementsByTagName("input");
		        	for(int i = 0; i < inputs.getLength(); i++) {
		        		Element input = (Element)inputs.item(i);
		        		
		        		String name = input.getAttribute("name");
		        		
		        		if (name != null) {
		        		if (name.equals("email")) {
		        			email = input;
		        		}
		        		if (name.equals("pass")) {
		        			pass = input;
		        		}}
		        		
		        	}
					
					if (email != null && pass != null) {
                        Properties props = new Properties();
                        try {
                            props.load(getClass().getResourceAsStream("user.properties"));
                        } catch (IOException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        email.setAttribute("value", props.getProperty("vk.login"));
						pass.setAttribute("value", props.getProperty("vk.password"));
//						webEngine.executeScript("document.forms['login'].submit();");
						webEngine.executeScript("document.forms[0].submit();");
						
						
					}
				}
				if (webEngine.getLocation().contains("http://m.vk.com/feed")) {
					System.out.println("Yep!");
					next.action = Action.CONTINUE;
					// HACK TO LOAD ONLY ONE EVENT
					next.state = State.LOAD_EVENTS;
					next.event = Event.INIT;
					List<String> list = new ArrayList<>();
					list.add("/event47914684");
					//list.add("/event49570234");
					//list.add("/totaldictdzr");
					next.eventData = list;
//					next.state = State.LOAD_LINKS;
//					next.event = Event.BROWSE;
//					next.eventData = "http://m.vk.com/search?c%5Bcity%5D=621&c%5Bcountry%5D=1&c%5Bsection%5D=communities&c%5Btype%5D=3";
				} else {
					next.action = Action.WAIT;
				}
			}
		});
		registerEventHandler(State.LOAD_LINKS, Event.LOADED, new EventHandler() {
			
			@Override
			public void execute() {
				try {
					JSObject window = (JSObject) webEngine.executeScript("window");
					window.setMember("app", new Object() {
						@SuppressWarnings("unused")
						public void finished() {
							System.out.println("Scrolling has been finished");
							next.action = Action.CONTINUE;
							next.event = Event.PARSE_LINKS;
							ContentFSM.this.run();
						}
						@SuppressWarnings("unused")
						public void log(int a, int b, int c) {
							System.out.println("a= " + a + "; b= " + b + "c= " + c);
						}
					});
					webEngine.executeScript("function rec() { app.log(document.body.scrollTop,document.body.offsetHeight,window.innerHeight); if (document.body.scrollTop < (document.body.offsetHeight - window.innerHeight)) {window.scrollBy(0, document.body.offsetHeight);setTimeout(rec, 2000);} else {app.finished();}}; rec();");
				}
				catch (JSException ex) {
					ex.printStackTrace();
				}
				next.action = Action.WAIT;
			}
		});
		registerEventHandler(State.LOAD_LINKS, Event.PARSE_LINKS, new EventHandler() {

			@Override
			public void execute() {
				Document doc = webEngine.getDocument();
				List<String> eventLinks = EventParser.getInstance().parseEventLinks(doc);
				//System.out.println(eventLinks.size());
				next.action = Action.CONTINUE;
				next.event = Event.INIT;
				next.state = State.LOAD_EVENTS;
				next.eventData = eventLinks;
			}
			
		});
		registerEventHandler(State.LOAD_EVENTS, Event.INIT, new EventHandler() {
			
			LinkedList<String> eventQueue;
			boolean queueSet = false;
			int count = 0;
			
			@SuppressWarnings("unchecked")
			@Override
			public void execute() {
				if (currentEventData != null) {
					System.out.println(new com.vk.fetch.Event(currentEventData));
				}
				if (++count > 2) {
					System.out.println("End! Event #" + count);
					System.out.println("----------------");
					next.action = Action.WAIT;
					return;
				}
				if (!queueSet) {
					eventQueue = new LinkedList<String>((List<String>) next.eventData);
					queueSet = true;
					System.out.println("LL: " + eventQueue.size());
				} 
				
				if (!eventQueue.isEmpty()) {
					String eventLink = eventQueue.removeFirst();
					next.action = Action.CONTINUE;
					next.event = Event.BROWSE;
					next.eventData = "http://m.vk.com" + eventLink;
				} else {
					System.out.println("End!");
					next.action = Action.WAIT;
				}
			}
			
		});
		registerEventHandler(State.LOAD_EVENTS, Event.LOADED, new EventHandler() {
			
			@Override
			public void execute() {
				Document doc = webEngine.getDocument();
				currentEventData = EventParser.getInstance().parseEvent(doc);
				next.state = State.LOAD_PHOTOS;
				next.event = Event.BROWSE;
				next.action = Action.CONTINUE;
				next.eventData = "http://m.vk.com" + currentEventData.linkToPhotoAlbum;
			}
		});
		registerEventHandler(State.LOAD_PHOTOS, Event.LOADED, new EventHandler() {

			@Override
			public void execute() {
				Document doc = webEngine.getDocument();
				currentEventData.photoLinks = EventParser.getInstance().parsePhotos(doc);
				
				next.state = State.LOAD_ATTENDEES;
				next.event = Event.BROWSE;
				next.action = Action.CONTINUE;
				next.eventData = "http://m.vk.com" + currentEventData.linkToAttendeeList;
			}
			
		});
		registerEventHandler(State.LOAD_ATTENDEES, Event.LOADED, new EventHandler() {

			@Override
			public void execute() {
				Document doc = webEngine.getDocument();
				currentEventData.attendeeLinks = EventParser.getInstance().parseAttendees(doc);
				
				next.state = State.LOAD_EVENTS;
				next.event = Event.INIT;
				next.action = Action.CONTINUE;
			}
			
		});
		
	}

	public State getCurrentState() {
		return next.state;
	}

	private void registerEventHandler(State state, Event event, EventHandler eventHandler) {
		Map<Event, EventHandler> stateEventHandlers = handlers.get(state);
		if (stateEventHandlers == null) {
			stateEventHandlers = new HashMap<Event, EventHandler>();
			handlers.put(state, stateEventHandlers);
		}
		stateEventHandlers.put(event, eventHandler);
	}
	
	private void registerEventHandler(Event event, EventHandler eventHandler) {
		for (State state : State.values()) {
			if (state != State.NULL) {
				registerEventHandler(state, event, eventHandler);
			}
		}
	}
}
interface EventHandler {
	void execute();
}

