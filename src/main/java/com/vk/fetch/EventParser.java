package com.vk.fetch;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class EventParser {
	private static EventParser instance = new EventParser();
	
	private XPath xpath;
	
	public static EventParser getInstance() {
		return instance;
	}
	
	private EventParser() {
		//init the xpath and set namespace context
		xpath = XPathFactory.newInstance().newXPath();
		xpath.setNamespaceContext(new NamespaceContext() {
			
			@Override
			public Iterator<String> getPrefixes(String namespaceURI) {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public String getPrefix(String namespaceURI) {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public String getNamespaceURI(String prefix) {
				if (prefix == null) throw new NullPointerException("Prefix mustn't be null!");
				else if (prefix.equals("pre")) return "http://www.w3.org/1999/xhtml";
				else if (prefix.equals("xml")) return XMLConstants.XML_NS_URI;
				return XMLConstants.NULL_NS_URI;
			}
		});
	}
	
	public EventData parseEvent(Document doc) {
		NodeList descriptionRowNodes = null;
		String attendeesText = null;
		
		//Map<String, String> descriptionMap = new HashMap<String, String>();
		String description = null;
		String startTime = null;
		String endTime = null;
		String website = null;
		String location = null;
		int attendeesCount;
		String photosLink = null;
		String attendeesLink = null;
		String profileImage = null;
		
		try {
			// get event description
			descriptionRowNodes = (NodeList) xpath.compile("//pre:dl[@class='pinfo_row']").evaluate(doc, XPathConstants.NODESET);
			
			for (int i = 0; i < descriptionRowNodes.getLength(); i++) {
				Node descriptionRow = descriptionRowNodes.item(i);
				String rowName = null;
				rowName = (String) xpath.evaluate("./pre:dt/text()", descriptionRow, XPathConstants.STRING);
				if (rowName == null || rowName.equals("")) continue;
				
				if (rowName.equals("Описание:")) {
					//DEBUG
					Node full = (Node) xpath.evaluate("./pre:dd/pre:span[@style='display:none']", descriptionRow, XPathConstants.NODE);
					System.out.println("-------------");
					System.out.println(innerHTML(full));
					System.out.println("-------------");
					//DEBUG END
					NodeList list = (NodeList) xpath.evaluate("./pre:dd/pre:span", descriptionRow, XPathConstants.NODESET);
					if (list.getLength() >= 2) {
						description = "";
						for (int j = 0; j < list.getLength(); j++) {
							description += formatText(innerHTML(list.item(j)));
						}
					} else {
						description = formatText(innerHTML((Node) xpath.evaluate("./pre:dd", descriptionRow, XPathConstants.NODE)));
					}
				} else if (rowName.equals("Веб-сайт:")) {
					website = (String) xpath.evaluate("./pre:dd/pre:a/text()", descriptionRow, XPathConstants.STRING);
				} else if (rowName.equals("Начало:")) {
					startTime = (String) xpath.evaluate("./pre:dd/text()", descriptionRow, XPathConstants.STRING);
				} else if (rowName.equals("Окончание:")) {
					endTime = (String) xpath.evaluate("./pre:dd/text()", descriptionRow, XPathConstants.STRING);
				} else if (rowName.equals("Место:")) {
					location = (String) xpath.evaluate("./pre:dd/pre:a/text()", descriptionRow, XPathConstants.STRING);
				}
			}
			
			// get attendees count
			attendeesText = (String) xpath.evaluate("//*[@id='mcont']/pre:div/pre:div[2]/pre:div[3]/pre:div/pre:ul[1]/pre:li[1]/pre:a/pre:em/text()", doc);
			attendeesCount = Integer.parseInt(attendeesText.replace("\n", ""));
			
			// get photo album link
			photosLink = (String) xpath.evaluate("//*[@id='mcont']/pre:div/pre:div[2]/pre:div[1]/pre:a/@data-href", doc, XPathConstants.STRING);
			
			// get attendees list link
			attendeesLink = (String) xpath.evaluate("//*[@id='mcont']/pre:div/pre:div[2]/pre:div[3]/pre:div/pre:ul[1]/pre:li[1]/pre:a/@data-href", doc, XPathConstants.STRING);
			
			// get event profile image
			//DEBUG
			Node nd = (Node) xpath.evaluate("//*[@id='mcont']/pre:div/pre:div[1]", doc, XPathConstants.NODE);
			System.out.println(innerHTML(nd));
			//DEBUG
			profileImage = (String) xpath.evaluate("//*[@id='mcont']/pre:div/pre:div[1]/pre:img/@src", doc, XPathConstants.STRING);
			
		} catch (XPathExpressionException e) {
			throw new RuntimeException("Invalid XPath expression!");
		}
		
		EventData data = new EventData();
		data.description = description;
		data.website = website;
		data.startTime = startTime;
		data.endTime = endTime;
		data.location = location;
		data.attendeesCount = attendeesCount;
		data.linkToPhotoAlbum = photosLink;
		data.linkToAttendeeList = attendeesLink;
		data.profileImage = profileImage;
		
		return data;
	}
	
	public List<String> parseEventLinks(Document doc) {
		List<String> eventHrefs = new ArrayList<String>();
		NodeList list = null;
		try {
			list = (NodeList) xpath.compile("//*[@id='mcont']/pre:div/pre:div[2]/pre:div[2]/pre:a/@data-href").evaluate(doc, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			throw new RuntimeException("Invalid XPath expression!");
		}
		if (list != null) {
			for (int i = 0; i < list.getLength(); i++) {
				eventHrefs.add(list.item(i).getNodeValue());
			}
		}
		return eventHrefs;
	}

	private String formatText(String text) {
		//return text.replaceAll("<BR[^>]*>", "\n").replaceAll("(\\r?\\n){2,}", "\n").trim();
		return text.replaceAll(" xmlns=\"[^\"]+\"", "").replaceAll("<A[^>]+>#[^<]+</A>", "").trim();
	}
	
	private String innerHTML(Node node) {
		if (node == null) return "[NULL]";
		StringWriter sw = new StringWriter();
		try {
			Transformer t = TransformerFactory.newInstance().newTransformer();
			t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			
			NodeList children = node.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				t.transform(new DOMSource(children.item(i)), new StreamResult(sw));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sw.toString();
	}

	public List<String> parsePhotos(Document doc) {
		List<String> photoLinks = new ArrayList<>();
		try {
			NodeList linkNodes = (NodeList) xpath.evaluate("//*[@id='mcont']/pre:div/pre:div[1]/pre:div/pre:a/@data-href", doc, XPathConstants.NODESET);
			for(int i = 0; i < linkNodes.getLength(); i++) {
				photoLinks.add(linkNodes.item(i).getNodeValue());
			}
		} catch (XPathExpressionException e) {
			throw new RuntimeException("Invalid XPath expression!");
		}
		return photoLinks;
	}
	
	
	public List<String> parseAttendees(Document doc) {
		List<String> attendeeLinks = new ArrayList<>();
		try {
			NodeList linkNodes = (NodeList) xpath.evaluate("//*[@id='mcont']/pre:div/pre:a/@data-href", doc, XPathConstants.NODESET);
			for(int i = 0; i < linkNodes.getLength(); i++) {
				attendeeLinks.add(linkNodes.item(i).getNodeValue());
			}
		} catch (XPathExpressionException e) {
			throw new RuntimeException("Invalid XPath expression!");
		}
		return attendeeLinks;
	}
	
}