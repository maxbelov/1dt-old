package com.vk.fetch;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker.State;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class VKEventFetcher extends Application {
	private Scene scene;
    @Override public void start(final Stage stage) {
        // create the scene
        stage.setTitle("Web View");
        final Browser browser = new Browser();
        scene = new Scene(browser,750,500, Color.web("#666970"));
        
        stage.setScene(scene);
        stage.show();
    }
    public static void main(String[] args) {
		launch(args);
	}
}

class Browser extends Region {
 
    WebView browser = new WebView();
    WebEngine webEngine = browser.getEngine();
    ContentFSM fsm = new ContentFSM(webEngine);
    

    public Browser() {
        //apply the styles
        getStyleClass().add("browser");
        // load the web page
        webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
        	
			@Override
			public void changed(ObservableValue<? extends State> observable,
					State oldValue, State newValue) {
				if (newValue == State.SUCCEEDED) {
					System.out.println("Loaded page URL: " + webEngine.getLocation());		
						fsm.pageLoaded();
						/*if (fsm.getCurrentState() == ContentFSM.State.AUTHENTICATE)
							fsm.pageLoaded();*/
				}
			}
		});
        
        fsm.run();
        //add the web view to the scene
        getChildren().add(browser);
 
    }
 
    @Override protected void layoutChildren() {
        double w = getWidth();
        double h = getHeight();
        layoutInArea(browser,0,0,w,h,0, HPos.CENTER, VPos.CENTER);
    }
 
    @Override protected double computePrefWidth(double height) {
        return 750;
    }
 
    @Override protected double computePrefHeight(double width) {
        return 500;
    }
}
