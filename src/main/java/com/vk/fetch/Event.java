package com.vk.fetch;

import java.io.StringWriter;
import java.util.List;

public class Event {
	List<String> attendees;
	List<String> photos;

	String description;
	String location;
	String startTime;
	String endTime;
	String website;
	String link;
	String profileImage;


	public Event(EventData data) {
		setDescription(data.description);
		setWebsite(data.website);
		setStartTime(data.startTime);
		setEndTime(data.endTime);
		setLocation(data.location);
		setAttendees(data.attendeeLinks);
		setPhotos(data.photoLinks);
		setProfileImage(data.profileImage);
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String place) {
		this.location = place;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getWebsite() {
		return website;
	}
	public void setWebsite(String website) {
		this.website = website;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public List<String> getAttendees() {
		return attendees;
	}
	public void setAttendees(List<String> attendees) {
		this.attendees = attendees;
	}
	public List<String> getPhotos() {
		return photos;
	}
	public void setPhotos(List<String> photos) {
		this.photos = photos;
	}

	public String toString() {
		StringWriter sw = new StringWriter();
		sw.append("----------------EVENT----------------------\n");
		sw.append(description.replaceAll("<[^<>]*>([^<>]*)<//[^<>]*>", ""));
		sw.append("\n");
		sw.append(startTime);
		sw.append("\n");
		sw.append(endTime);
		sw.append("\n");
		sw.append(website);
		sw.append("\n");
		sw.append(location);
		sw.append("\n");
		sw.append(profileImage);
		sw.append("\n");
		sw.append(photos.toString());
		sw.append("\n");
		sw.append(attendees.toString());
		sw.append("\n----------------EVENT END------------------\n");
		
		return sw.toString();
	}
}
